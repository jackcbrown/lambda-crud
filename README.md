# lambda CRUD

node.js lambda that performs CRUD operations on [PostBin](https://postb.in/). Deployed with [serverless](https://serverless.com/). Executed with a CloudWatch Event.

## Setup

Start by cloning the repository and installing the dependencies:

```sh
git clone https://gitlab.com/jackcbrown/lambda-crud.git
cd lambda-crud
npm install
```

## Deployment

To deploy this project to an AWS account using the serverless framework, you'll need to [specify your AWS credentials](https://serverless.com/framework/docs/providers/aws/guide/credentials/).

## Local Development

To run this project locally, you'll need to install [AWS SAM](https://aws.amazon.com/serverless/sam/).

Once installed, you can invoke the function with `sam local invoke crud --no-event`

_Note: SAM runs the transpiled code. Don't forget to use `npm run build` first!_
