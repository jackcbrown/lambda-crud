module.exports = {
  verbose: true,
  silent: true,
  clearMocks: true,
  restoreMocks: true,
  collectCoverage: true
};
