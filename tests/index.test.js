import { handler } from '../src/index';
import PostBinClient from '../src/PostBin';
import { getRandomQuote } from '../src/utils';

jest.mock('../src/PostBin').mock('../src/utils');

describe('handler', () => {
  test('creates postBin client', async () => {
    await handler();
    expect(PostBinClient).toHaveBeenCalledTimes(1);
  });

  test('calls postBin.create with random quote', async () => {
    const fakeQuote = 'hello world';
    getRandomQuote.mockResolvedValue(fakeQuote);

    await handler();
    expect(PostBinClient.prototype.create).toHaveBeenCalledWith({
      quote: fakeQuote
    });
  });

  test('calls each CRUD operation once on postBin client', async () => {
    await handler();
    expect(PostBinClient.prototype.create).toHaveBeenCalledTimes(1);
    expect(PostBinClient.prototype.read).toHaveBeenCalledTimes(1);
    expect(PostBinClient.prototype.update).toHaveBeenCalledTimes(1);
    expect(PostBinClient.prototype.delete).toHaveBeenCalledTimes(1);
  });

  test('returns binUrl', async () => {
    const fakeBinUrl = 'https://postb.in/b/123';

    PostBinClient.prototype.binUrl = fakeBinUrl;
    await expect(handler()).resolves.toBe(fakeBinUrl);
  });

  test('throws error if PostBin throws an error', async () => {
    const fakeErrorMessage = 'test error';

    PostBinClient.prototype.createBin.mockRejectedValue(
      new Error(fakeErrorMessage)
    );

    await expect(handler()).rejects.toThrowError(fakeErrorMessage);
  });
});
