import axios from 'axios';

import PostBinClient from '../src/PostBin';

jest.mock('axios');

const postBinBaseUrl = 'https://postb.in';

describe('PostBinClient', () => {
  const fakeBinId = 'abc123';
  const postBin = new PostBinClient();
  test('createBin should call axios.post and return bin id', async () => {
    axios.post.mockResolvedValue({
      data: {
        binId: fakeBinId
      }
    });
    const binId = await postBin.createBin();
    expect(axios.post).toHaveBeenCalledTimes(1);
    expect(binId).toBe(fakeBinId);
  });

  test('deleteBin should call axios.delete and return response data', async () => {
    const fakeDeleteResponseData = {
      msg: 'bin deleted'
    };
    axios.delete.mockResolvedValue({
      data: fakeDeleteResponseData
    });
    const data = await postBin.deleteBin();
    expect(axios.delete).toHaveBeenCalledTimes(1);
    expect(data).toBe(fakeDeleteResponseData);
  });

  test('binUrl should contain the binId and postBin url', () => {
    postBin.binId = fakeBinId;
    expect(postBin.binUrl).toBe(`${postBinBaseUrl}/b/${fakeBinId}`);
  });

  describe('CRUD operations', () => {
    const fakeRequestId = '123456';
    postBin.binId = fakeBinId;

    test('create should call axios.post with request data and return request id', async () => {
      const fakeCreateData = { someData: 'ok' };
      axios.post.mockResolvedValue({
        data: fakeRequestId
      });
      const requestId = await postBin.create(fakeCreateData);
      expect(axios.post).toHaveBeenCalledTimes(1);
      expect(axios.post).toHaveBeenCalledWith(
        expect.any(String),
        fakeCreateData
      );
      expect(requestId).toBe(fakeRequestId);
    });

    test('read should call axios.get with query params and return request id', async () => {
      const fakeQueryParams = {
        limit: 200
      };
      axios.get.mockResolvedValue({
        data: fakeRequestId
      });
      const requestId = await postBin.read(fakeQueryParams);
      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(expect.any(String), {
        params: fakeQueryParams
      });
      expect(requestId).toBe(fakeRequestId);
    });

    test('update should call axios.put with request data and return request id', async () => {
      const fakeUpdateData = { someData: 'ok' };
      axios.put.mockResolvedValue({
        data: fakeRequestId
      });
      const requestId = await postBin.update(fakeUpdateData);
      expect(axios.put).toHaveBeenCalledTimes(1);
      expect(axios.put).toHaveBeenCalledWith(
        expect.any(String),
        fakeUpdateData
      );
      expect(requestId).toBe(fakeRequestId);
    });

    test('delete should call axios.delete and return request id', async () => {
      axios.delete.mockResolvedValue({
        data: fakeRequestId
      });
      const requestId = await postBin.delete();
      expect(axios.delete).toHaveBeenCalledTimes(1);
      expect(requestId).toBe(fakeRequestId);
    });
  });
});
