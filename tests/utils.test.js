import axios from 'axios';

import { getRandomQuote } from '../src/utils';

jest.mock('axios');

describe('getRandomQuote', () => {
  test('calls axios once and returns the quoteText', async () => {
    const fakeQuoteText = 'hello world';
    axios.get.mockResolvedValue({
      data: {
        quoteText: fakeQuoteText
      }
    });
    await expect(getRandomQuote()).resolves.toBe(fakeQuoteText);
    expect(axios.get).toHaveBeenCalledTimes(1);
  });
});
