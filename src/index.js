import PostBinClient from './PostBin';
import { getRandomQuote } from './utils';

/**
 * Runs CRUD operations sequentially on the https://postb.in API
 */
export const handler = async () => {
  try {
    const postBin = new PostBinClient();
    const binId = await postBin.createBin();
    postBin.binId = binId;

    const quote = await getRandomQuote();
    await postBin.create({ quote });

    await postBin.read({ name: 'jim', limit: 20 });

    await postBin.update({ hello: 'world' });

    await postBin.delete();

    const binUrl = postBin.binUrl;

    console.log('See the bin here:', binUrl);
    return binUrl;
  } catch (err) {
    console.error(err);
    throw err;
  }
};
