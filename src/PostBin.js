import axios from 'axios';

const postBinBaseUrl = 'https://postb.in';

/**
 * @returns PostBin API Client
 */
export default class PostBinClient {
  constructor() {
    this.binId = '';
  }

  /**
   * @returns {Promise<String>} promise containing id of newly created bin
   */
  async createBin() {
    const response = await axios.post(`${postBinBaseUrl}/api/bin`);
    return response.data.binId;
  }

  /**
   * @returns {Promise<Object>} promise containing response data from postbin
   */
  async deleteBin() {
    const response = await axios.delete(
      `${postBinBaseUrl}/api/bin/${this.binId}`
    );
    return response.data;
  }

  /**
   * @returns {Promise<String>} promise containing id of this request id
   */
  async create(data) {
    const response = await axios.post(`${postBinBaseUrl}/${this.binId}`, data);
    const requestId = response.data;
    return requestId;
  }

  /**
   * @param queryParams {Object} object containing query string parameters. E.g. { limit: 300 }
   * @returns {Promise<String>} promise containing id of this request id
   */
  async read(queryParams) {
    const response = await axios.get(`${postBinBaseUrl}/${this.binId}`, {
      params: queryParams
    });
    const requestId = response.data;
    return requestId;
  }

  /**
   * @returns {Promise<String>} promise containing id of this request id
   */
  async update(data) {
    const response = await axios.put(`${postBinBaseUrl}/${this.binId}`, data);
    const requestId = response.data;
    return requestId;
  }

  /**
   * @returns {Promise<String>} promise containing id of this request id
   */
  async delete() {
    const response = await axios.delete(`${postBinBaseUrl}/${this.binId}`);
    const requestId = response.data;
    return requestId;
  }

  get binUrl() {
    return `${postBinBaseUrl}/b/${this.binId}`;
  }
}
