import axios from 'axios';

const quotesBaseUrl = 'http://api.forismatic.com/api/1.0/';

export const getRandomQuote = async () => {
  const response = await axios.get(quotesBaseUrl, {
    params: {
      method: 'getQuote',
      format: 'json',
      lang: 'en'
    }
  });

  const quote = response.data.quoteText;
  return quote;
};
